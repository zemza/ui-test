//const { Browser } = require("selenium-webdriver");
//import chai from 'chai';
import { expect } from 'chai';
import { Browser, Builder, By, Key, WebElement } from "selenium-webdriver";
//import firefox from "selenium-webdriver/firefox";
import { WebDriver } from 'selenium-webdriver';
import firefox from "selenium-webdriver/firefox.js"
//import { Options } from 'selenium-webdriver/firefox';
import "geckodriver";
//import firefox from 'org.openqa.selenium.firefox.FirefoxDriver';
const BASE_URL = "https://niisku.lab.fi/~kardev/projects/color-picker";
const sleep = (seconds) => new Promise((resolve) => setTimeout(resolve, seconds *1000));

describe("UI Tests", () => {
    /**@type {import("selenium-webdriver").ThenableWebDriver}*/
    let driver = undefined;
    before(async () => {
        let options = new firefox.Options();
        options.setBinary("~/snap/firefox/firefox");
        driver = await new Builder().forBrowser(Browser.FIREFOX).setFirefoxOptions(options).build();
        //driver.set_page_load_timeout(5)
        await driver.get(BASE_URL);
        await sleep(2);
        
    });
    it("Can find all rgb sliders", async () => {
        const slider_r = await driver.findElement(By.id("slider-r"));
        const slider_g = await driver.findElement(By.id("slider-g"));
        const slider_b = await driver.findElement(By.id("slider-b"));
    });
    it("Can find all rgb inputs", async () => {
        const val_r = await driver.findElement(By.id("value-r"));
        const val_g = await driver.findElement(By.id("value-g"));
        const val_b = await driver.findElement(By.id("value-b"));
    });
    it("Can get hex value from 'value-hex' input field", async () => {
        const hex = await driver.findElement(By.id("value-hex")).getAttribute("value");
        expect(hex).to.eq("#00ff00");
    });
    it("Can get and set 'value-rgb' input field", async () => {
        const box_rgb = await driver.findElement(By.id("value-rgb"))
        let val_rgb = await box_rgb.getAttribute("value");
        expect(val_rgb).to.eq("rgb(0, 255, 0)");
        // modify value
        const keystroke_sequence = [];
        for (let i = 0; i < val_rgb.length; i++) {
            keystroke_sequence.push(Key.BACK_SPACE);
        }
        keystroke_sequence.push("rgb(255,255,0)");
        keystroke_sequence.push(Key.ENTER);
        await box_rgb.sendKeys(...keystroke_sequence);
        val_rgb = await box_rgb.getAttribute("value");
        expect(val_rgb).to.eq("rgb(255, 255, 0)");
        await sleep(2);
    });
    it("Can move slider", async () => {
        const slider_r = await driver.findElement(By.id("slider-r"));
        const slider_g = await driver.findElement(By.id("slider-g"));
        const slider_b = await driver.findElement(By.id("slider-b"));
        await slider_r.sendKeys(Key.LEFT.repeat(255));
        await slider_g.sendKeys(Key.LEFT.repeat(255));
        await slider_b.sendKeys(Key.RIGHT.repeat(255));
        await sleep(2);
        const hex = await driver.findElement(By.id("value-hex")).getAttribute("value");
        expect(hex).to.eq("#0000ff");
    });
    after(async () => {
        await driver.quit();
    });
});